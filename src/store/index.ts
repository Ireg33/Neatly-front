import Vue from 'vue'
import Vuex from 'vuex'
import router from "@/router";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    accessToken: localStorage.getItem('accessToken') || null,
    refreshToken: localStorage.getItem('refreshToken') || null,
    permissions: JSON.parse(localStorage.getItem('permissions') || '[]') || [],
    roles: JSON.parse(localStorage.getItem('roles') || '[]') || [],
    user_id: localStorage.getItem('user_id') || null,
  },
  getters: {
    loggedIn({ accessToken }): boolean {
      return !!accessToken
    },
    accessToken({accessToken}): string | null {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return accessToken
    },
    permissions({permissions}):string[] | null{
      return permissions;
    },
    userId({user_id}){
      return user_id;
    }
  },
  mutations: {
    LOGIN(state: any, authenticationData: any) {
      state.accessToken = authenticationData.accessToken
      state.refreshToken = authenticationData.refreshToken
      state.permissions = authenticationData.permissions
      state.roles = authenticationData.roles
      state.user_id = authenticationData.user_id
    },
    LOGOUT(state: any): void {
      state.accessToken = ''
      state.refreshToken = ''
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
      localStorage.removeItem('permissions')
      localStorage.removeItem('roles')
      localStorage.removeItem('user_id')
    },
  },
  actions: {
    login: async({ commit }: any, payload: any): Promise<void> => {
      const data = payload.data.data;
      const authenticationData = {
        accessToken: data.access_token,
        refreshToken: data.refresh_token,
        permissions: data.user.permissions,
        roles: data.user.roles,
        user_id: data.user.user_id,
      }
      localStorage.setItem('accessToken', authenticationData.accessToken)
      localStorage.setItem('refreshToken', authenticationData.refreshToken)
      localStorage.setItem('permissions', JSON.stringify(authenticationData.permissions))
      localStorage.setItem('roles', JSON.stringify(authenticationData.roles))
      localStorage.setItem('user_id', authenticationData.user_id)
      commit('LOGIN',authenticationData)
    },
    logout: async ({ commit }): Promise<void> => {
      commit('LOGOUT')
      await router.push({ name: 'Login' })
    },
  },
})
