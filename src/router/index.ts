import Vue from 'vue'
import store from '@/store'
import VueRouter, { RouteConfig } from 'vue-router'
import hasPermission from "@/utils/hasPermission";

Vue.use(VueRouter)

export const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home-page.vue'),
    meta: { layout: 'LoggedInLayout', isHome: true},
  },
  {
    path: '/weekly-plans',
    name: 'weekly-plans',
    component: () => import(/* webpackChunkName: "about" */ '../views/WeeklyPlan.vue'),
    meta: {
      requiresAuth: true,
      layout: 'LoggedInLayout',
      permission: 'view_weekly_plans',
      drawer:
          {
            label: 'Weekly Plans',
            icon: 'mdi-notebook'
          },
    },
  },
  {
    path: '/users',
    name: 'users',
    component: () => import(/* webpackChunkName: "about" */ '../views/users/UsersList.vue'),
    meta: {
      requiresAuth: true,
      layout: 'LoggedInLayout',
      permission: 'view_users',
      drawer:
          {
            label: 'Users',
            icon: 'mdi-account'
          },
    },
  },
  {
    path: '/schedule',
    name: 'schedule',
    component: () => import(/* webpackChunkName: "about" */ '../views/schedule/SchedulePage.vue'),
    meta: {
      requiresAuth: true,
      layout: 'LoggedInLayout',
      permission: 'view_schedule',
      drawer:
          {
            label: 'Schedule',
            icon: 'mdi-calendar-arrow-right'
          },
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login-page.vue'),
    meta: { requiresVisitor: true, layout: 'AuthLayout' },
  }
]

export const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(({ meta }) => meta.requiresVisitor) && store.getters.loggedIn) next({ name: 'home' })
  else if (to.matched.some(({ meta }) => meta.isHome) && !store.getters.loggedIn) next({ name: 'Login' })
  else if (
      to.matched.some(({ meta }) => meta.requiresAuth && !hasPermission(meta.permission)) &&
      store.getters.loggedIn
  )
    next({name: 'home'})
  else if (to.matched.some(({ meta }) => meta.requiresAuth) && !store.getters.loggedIn) next({ name: 'Login' })
  else
    next()
})

export default router
