import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  options: {
    customProperties: true,
  },
  icons: {
    iconfont: "mdi",
  },
  theme: {
    themes: {
      light: {
        primary: "#D6D9CE",
        secondary: "#2C2B22",
        s_accent: "#656450",
        error: "#b71c1c",
      },
    },
  },
});
