import store from '@/store'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import isArray from 'lodash/isArray'

export default (permission: string) => {
    if (isArray(store.getters.permissions))
        return store.getters.permissions.map(({ name }: { name: string }) => name).includes(permission)
    return false
}
